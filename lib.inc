%define STDOUT_DESCRIPTOR 1
%define WRITE_SYSCALL_NUMBER 1
%define EXIT_SYSCODE_NUMBER 60
%define NULL_SYMBOL 0 
%define LENGTH_OF_CHAR 1 
%define RADIX 10 
%define ASCII_OFFSET 48

section .text
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_SYSCODE_NUMBER
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax          ; Обнуляем счетчик длины строки
    .loop:               
        cmp byte [rdi+rax], NULL_SYMBOL   ; Сравниваем символ с нулевым байтом для определения конца строки
        je .end                 ; Если символ равен нулю, переходим к метке .end
        inc rax                 ; Увеличиваем счетчик
        jmp .loop               ; Переходим к следующей итерации цикла
    .end:
        ret                     ; Возвращаем значение длины строки

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi                 
    call string_length
    pop rdi
    mov rsi, rdi               
    mov rdx, rax                ; Помещаем длину строки из string_length в rdx
    mov rax, WRITE_SYSCALL_NUMBER
    mov rdi, STDOUT_DESCRIPTOR
    syscall                     ; Вызов системного вызова write
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi                    ; Cохраняем rdi
    mov rsi, rsp                ; передаем адрес символа
    pop rdi 
    mov rdx, 1
    mov rdi, STDOUT_DESCRIPTOR
    mov rax, WRITE_SYSCALL_NUMBER
    syscall                     ; Вызов системного вызова write
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, `\n`                 ; 10 - это код сивола перевода строки
    jmp print_char               ; Переход на функцию print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax 
    mov r8, rsp                  ; Сохраняем в r8 начальное значения PS
    mov r9, RADIX                   ; Сохраняем в r9 основание сс
    mov rax, rdi                   
    push 0                       ; Ноль, так как строка нуль-терминированная
    .loop:
        xor rdx,rdx              ; Очищаем rdx
        div r9
        add rdx, ASCII_OFFSET              ; Конвертируем символ в ASCII
        dec rsp                    
        mov byte[rsp], dl        ; Сохраняем символ в стеке
        test rax, rax               ; Проверяем, дошли ли мы до конца строки
        je .end
        jmp .loop
    .end:
        mov rdi, rsp             ; Загружаем в rdi адрес переведенного числа
        push r8                  ; Сохраняем r8
        call print_string
        pop r8
        mov rsp, r8              ; Возвращаем изначальное значение в указатель стека
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    test rdi, rdi
    jl .else                        
    jmp print_uint               ; Если число неотрицательно, переходим на print_uint, иначе переходим на else
    .else:
        push rdi                 ; Сохраняем аргумент
        mov rdi, `-`               ; Записываем код символа минуса в ASCII в rdi
        call print_char          ; Выводим символ минус
        pop rdi
        neg rdi                  ; Меняем знак
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx
    xor r8, r8
    xor r9, r9
    .loop:                       ; rcx - счётчик, rdi и rsi - указатели, r8b и r9b - регистры для i-ого элемента строк, младшие 8 бит
        mov r8b, byte[rcx+rdi]
        mov r9b, byte[rcx+rsi]
        cmp r8b, r9b             ; Сравниваем два символа
        jne .else                ; Если они не равны, переходим на .else
        cmp r8b, NULL_SYMBOL               ; Проверка на конец строки
        je .then
        inc rcx
        jmp .loop
    .then:
        mov rax, 1               ; Возвращаем результат в случае, если строки равны
        ret
    .else:
        mov rax, 0               ; Возвращаем результат в случае, если строки не равны
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi                 ; Очищаем rdi (0 - дескриптор stdin)
    mov rdx, LENGTH_OF_CHAR
    push NULL_SYMBOL
    mov rsi, rsp                 ; Загружаем адрес символа
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax
    xor rcx, rcx                    ; rcx - счётчик, rsi - размера буфера, rdi - адрес начала
    push rsi
    push rdi
    .loop:
        push rcx
        call read_char
        pop rcx
        mov rdi, [rsp]              ; Восстанавливаем rdi
        mov rsi, [rsp+8]            ; Восстанавливаем rsi
        test rax, rax               ; Проверка конца строки
        je .end
        cmp rax, `\n`               ; Проверка символа перевода строки
        je .spaces
        cmp rax, ` `               ; Проверка пробельного символа
        je .spaces
        cmp rax, `\t`               ; Проверка символа табуляции
        je .spaces
        mov [rcx+rdi], rax          ; Загружаем символ
        inc rcx
        cmp rcx, rsi                ; Сравнение длины слова с размером буфера
        jge .out_of_range
        jmp .loop       
    .spaces:
        test rcx, rcx               ; Ситуация, когда пробел находится в начале
        je .loop
        jmp .end                    ; Ситуация, когда пробел находится в конце
    .out_of_range:
        xor rax, rax
        xor rdx, rdx
        pop rdi
        pop rsi
        ret
    .end:
        xor rax, rax
        mov [rcx+rdi], rax          ; Дописываем ноль-терминатор
        mov rax, rdi                
        mov rdx, rcx
        pop rdi
        pop rsi
        ret 
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx                    ; rcx - счётчик длины слова
    mov r8, RADIX                      ; Сохраняем в r8 основание cc
    .loop:
        movzx r9, byte[rdi+rcx]     ; В r9 загружаем байт с расширением разрядности без учета знака
        test r9, r9                   ; Производим проверку на конец строки
        je .end
        cmp r9b, `0`                
        jl .end   
        cmp r9b, `9`                ; Проверка на диапозону от 0(48) до 9(57)
        jg .end
        mul r8 
        sub r9b, `0`                ; Получаем код цифры
        add rax, r9                 ; Добавляем цифру после сдвинутого разряда
        inc rcx 
        jmp .loop
    .end
        mov rdx, rcx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov rcx, rdi
    xor rdx, rdx
    cmp byte[rcx], `-`              ; Проверяем числа на знак, если он отрицательный, то выводим минус и вызываем print_uint
    je .then
    jmp .else
    .then:                         ; Если число отрицательное
        inc rcx                    ; Сдвигаем адрес начала числа, т.к. минус
        mov rdi, rcx
        push rcx                   ; Сохраняем rcx
        call parse_uint
        pop rcx
        neg rax
        inc rdx
        ret
    .else:                         ; Если число положительное
        mov rdi, rcx
        jmp parse_uint            

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx
    push rdx
    push rdi
    push rsi
    push rcx
    call string_length              ; Находим длину строки
    pop rcx
    pop rsi
    pop rdi
    pop rdx
    mov r8, rax                     ; Сохраняем длину строки в регистр r8
    cmp rdx, r8                     ; Сравниваем размер буфера и длину строки, возвращаем 0
    jl .out_of_range
    .loop:
        cmp rcx, r8                 ; При выходе за пределы строки
        jg .end
        mov r10, [rcx+rdi]          ; Копируем символ строки
        mov [rcx+rsi], r10          ; Помещаем символ строки в регистр r10 и оттуда вставляем в буфер
        inc rcx
        jmp .loop
    .out_of_range:
        xor rax, rax                  ; Возвращаем ноль, если строка не умещается в буфер
        ret
    .end:
        mov rax, r8
        ret
